import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:practicafinal/pages/compras/compras.page.dart';
import 'package:practicafinal/pages/inicio/inicio.page.dart';
import 'package:practicafinal/pages/novedades.page.dart';
import 'package:practicafinal/pages/perfil.page.dart';
import 'package:practicafinal/widgets/boton_icono.widget.dart';
import 'package:practicafinal/widgets/boton_navegacion.widget.dart';

void main() {
  runApp(
    //DevicePreview(enabled: true, builder: (context) => const MyApp())
    const MyApp(),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int nIndex = 0;
  final oPages = [
    const InicioPage(),
    const NovedadesPage(),
    const ComprasPage(),
    const PerfilPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              Container(
                //   color: Colors.red,
                //Color.fromRGBO(250, 250, 254, 1.0),
                child: oPages[nIndex],
              ),
              BotonNavegacion(
                nIndex: nIndex,
                oBoton: [
                  const SizedBox(
                    height: 20,
                  ),
                  BotonIcono(
                    nIconSize: 30.00,
                    oAccionBoton: () {
                      setState(() {
                        nIndex = 0;
                      });
                    },
                    oIcono:
                        Icon((nIndex == 0 ? Icons.home : Icons.home_outlined)),
                    lSeleccionado: (nIndex == 0 ? true : false),
                  ),
                  BotonIcono(
                    nIconSize: 30.00,
                    oAccionBoton: () {
                      setState(() {
                        nIndex = 2;
                      });
                    },
                    oIcono: Icon((nIndex == 2
                        ? Icons.shopping_cart
                        : Icons.shopping_cart_outlined)),
                    lSeleccionado: (nIndex == 2 ? true : false),
                  ),
                  BotonIcono(
                    nIconSize: 30.00,
                    oAccionBoton: () {
                      setState(() {
                        nIndex = 1;
                      });
                    },
                    oIcono: Icon((nIndex == 1
                        ? Icons.view_list
                        : Icons.view_list_outlined)),
                    lSeleccionado: (nIndex == 1 ? true : false),
                  ),
                  BotonIcono(
                    nIconSize: 30.00,
                    oAccionBoton: () {
                      setState(() {
                        nIndex = 3;
                      });
                    },
                    oIcono: Icon(
                        (nIndex == 3 ? Icons.person : Icons.person_outline)),
                    lSeleccionado: (nIndex == 3 ? true : false),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              )
            ],
          ),
          /*  bottomNavigationBar: Container(
              height: 120,
              color: Colors.amber,
              child: NavigationBar(destinations: const [
                NavigationDestination(
                    icon: Icon(Icons.access_time), label: 'prueba'),
                NavigationDestination(
                    icon: Icon(Icons.access_time), label: 'prueba'),
                NavigationDestination(
                    icon: Icon(Icons.access_time), label: 'prueba'),
                NavigationDestination(icon: Icon(Icons.access_time), label: '')
              ]),
            )*/
        ),
      ),
    );
  }
}
