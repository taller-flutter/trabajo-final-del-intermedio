import 'package:flutter/material.dart';

class ColorsApp {
  static const oColorFondo = Color.fromRGBO(239, 240, 249, 1.0);
  static const oColorNavegacion = Color.fromRGBO(223, 220, 221, 1.0);
  static const oColorGrisOscuro = Color.fromRGBO(178, 177, 182, 1.0);
  static const oColorGrisClaro = Color.fromRGBO(231, 233, 237, 1.0);
  static const oColorAlterno = Color.fromRGBO(253, 176, 188, 1.0);
  static const oColorAlternoGradiente = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      Color.fromRGBO(253, 176, 188, 1.0),
      Color.fromRGBO(236, 129, 145, 1.0),
    ],
  );
}
