import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../themes/colors_app.theme.dart';

class BotonIcono extends StatefulWidget {
  const BotonIcono(
      {super.key,
      required this.nIconSize,
      required this.oAccionBoton,
      required this.oIcono,
      required this.lSeleccionado});

  final double nIconSize;
  final VoidCallback oAccionBoton;
  final Icon oIcono;
  final bool lSeleccionado;

  @override
  State<BotonIcono> createState() => _BotonIconoState();
}

class _BotonIconoState extends State<BotonIcono> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: (widget.lSeleccionado ? ColorsApp.oColorAlterno : null),
        borderRadius: BorderRadius.circular(40.00),
      ),
      child: IconButton(
          color: (widget.lSeleccionado
              ? ColorsApp.oColorGrisClaro
              : ColorsApp.oColorGrisOscuro),
          iconSize: widget.nIconSize,
          onPressed: widget.oAccionBoton,
          icon: widget.oIcono),
    );
  }
}
