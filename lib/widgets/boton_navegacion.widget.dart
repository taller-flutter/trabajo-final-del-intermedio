import 'package:flutter/material.dart';

import '../themes/colors_app.theme.dart';
import 'boton_icono.widget.dart';

class BotonNavegacion extends StatefulWidget {
  const BotonNavegacion({
    Key? key,
    required this.oBoton,
    required int nIndex,
  }) : super(key: key);

  final List<Widget> oBoton;

  @override
  State<BotonNavegacion> createState() => _BotonNavegacionState();
}

class _BotonNavegacionState extends State<BotonNavegacion> {
  @override
  Widget build(BuildContext context) {
    final nSize = MediaQuery.of(context).size;
    return Padding(
      padding:
          const EdgeInsets.only(top: 320, left: 20, right: 20, bottom: 20.00),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          height: nSize.height * 0.10,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: ColorsApp.oColorNavegacion,
            //const Color.fromRGBO(238, 238, 238, 1.0),
            borderRadius: BorderRadius.circular(45.00),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: widget
                .oBoton, /*[
              const SizedBox(
                height: 20,
              ),
             
              IconButton(
                  iconSize: 30.00,
                  onPressed: widget.oAccionBoton,
                  icon: const Icon(Icons.home_outlined)),
              IconButton(
                  iconSize: 30.00,
                  onPressed: widget.oAccionBoton,
                  icon: const Icon(Icons.view_list_outlined)),
              IconButton(
                  iconSize: 30.00,
                  onPressed: () {},
                  icon: const Icon(Icons.shopping_cart_outlined)),
              IconButton(
                  iconSize: 30.00,
                  onPressed: () {},
                  icon: const Icon(Icons.person_outline)),
              const SizedBox(
                height: 20,
              ),
            ],*/
          ),
        ),
      ),
    );
  }
}
