import 'package:flutter/material.dart';

import '../themes/colors_app.theme.dart';

class Cabecera extends StatelessWidget {
  const Cabecera({
    Key? key,
    required this.nSize,
  }) : super(key: key);

  final Size nSize;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(top: nSize.height * 0.02, left: 20.00, right: 20.00),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30.00),
              ),
              child: IconButton(
                color: ColorsApp.oColorAlterno,
                iconSize: 30.00,
                onPressed: () {},
                icon: const Icon(Icons.grid_view_rounded),
              )),
          Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text('HOLA'),
              Text(
                'LUCERO LUANA',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          const CircleAvatar(
            radius: 23,
            backgroundImage: NetworkImage(
                'https://thumbs.dreamstime.com/b/artista-de-sexo-femenino-hermoso-en-su-estudio-59656912.jpg'),
          ),
        ],
      ),
    );
  }
}
