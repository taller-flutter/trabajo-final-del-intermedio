import 'package:flutter/material.dart';

import '../../../themes/colors_app.theme.dart';

class Total extends StatelessWidget {
  const Total({
    Key? key,
    required this.nSize,
    required this.nTotal,
  }) : super(key: key);

  final Size nSize;
  final int nTotal;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(top: nSize.height * 0.6, left: 20.00, right: 20.00),
      child: Container(
        // color: Colors.red,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                const Text(
                  'Total',
                  style: TextStyle(
                    fontSize: 18,
                    color: ColorsApp.oColorGrisOscuro,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "S/ ${nTotal.toString()}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Container(
              width: 120,
              height: 50,
              //nSize.height * 0.15,
              decoration: BoxDecoration(
                  gradient: ColorsApp.oColorAlternoGradiente,
                  borderRadius: BorderRadius.circular(30.00)),
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  'Pagar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
