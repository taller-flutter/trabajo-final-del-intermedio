import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../models/models.dart';
import '../../themes/colors_app.theme.dart';
import '../../widgets/cabecera.widget.dart';
import 'widgets/total.widget.dart';

class ComprasPage extends StatefulWidget {
  const ComprasPage({super.key});

  @override
  State<ComprasPage> createState() => _ComprasPageState();
}

class _ComprasPageState extends State<ComprasPage> {
  @override
  void initState() {
    obtenerProducto("1");

    super.initState();
  }

  List<Producto> oListaProducto1 = [];
  List<Producto> oListaProducto = [];
  int lnTotal = 0;
  void obtenerProducto(String nCategoria) async {
    var dio = Dio();
    final response = await dio
        .get('https://api.escuelajs.co/api/v1/categories/$nCategoria/products');

    // oListaEmpresa:List<Empresa>.from(oNuevaListaEmpresa.map((x) => Empresa.fromJson(x))),

    oListaProducto1 =
        List<Producto>.from(response.data.map((x) => Producto.fromJson(x)));

    oListaProducto = oListaProducto1
        .where((element) => oListaProducto1.indexOf(element) < 3)
        .toList();

    oListaProducto.forEach((element) => lnTotal = lnTotal + element.price!);

    if (this.mounted) {
      setState(() {});
    }

    //setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final nSize = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          color: ColorsApp.oColorFondo,
        ),
        Cabecera(nSize: nSize),
        Padding(
          padding:
              EdgeInsets.only(top: nSize.height * 0.10, left: 20, right: 20),
          child: (oListaProducto.isNotEmpty
              ? SingleChildScrollView(
                  child: SizedBox(
                  height: nSize.height * 0.6,
                  child: ListView.builder(
                      //scrollDirection: Axis.horizontal,
                      itemCount: oListaProducto.length,
                      itemBuilder: ((context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Container(
                            width: double.infinity,
                            height: 120,
                            //color: Colors.amber,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15.00),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10.00, horizontal: 10.0),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ClipRRect(
                                      borderRadius:
                                          BorderRadius.circular(15.00),
                                      child: Image(
                                        width: 80,
                                        height: 100,
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                            oListaProducto[index]
                                                .images![0]
                                                .toString()),
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        Flexible(
                                          child: Text(
                                              //textAlign: TextAlign.start,
                                              oListaProducto[index]
                                                  .title
                                                  .toString(),
                                              style: const TextStyle(
                                                  // color: Colors.grey,
                                                  fontSize: 16.00,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        Flexible(
                                          child: Text(
                                              //textAlign: TextAlign.start,
                                              oListaProducto[index]
                                                  .category!
                                                  .name
                                                  .toString(),
                                              style: const TextStyle(
                                                  color: ColorsApp
                                                      .oColorGrisOscuro,
                                                  fontSize: 15.00,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        Flexible(
                                          child: Text(
                                              "S/  ${oListaProducto[index].price.toString()}",
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18.00,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          decoration: const BoxDecoration(
                                              color: ColorsApp.oColorAlterno,
                                              //Color.fromRGBO(253, 176, 188, 1.0),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5.0)),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black12,
                                                    blurRadius: 15,
                                                    offset: Offset(0, 5))
                                              ]),
                                          child: IconButton(
                                            iconSize: 25.00,
                                            color: Colors.white,
                                            onPressed: () {},
                                            icon: const Icon(Icons.check),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 20.0,
                                        ),
                                        Row(
                                          children: const [
                                            Text('+',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 15.00,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            SizedBox(
                                              width: 20.0,
                                            ),
                                            Text('1',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 15.00,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            SizedBox(
                                              width: 15.0,
                                            ),
                                            Text('-',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 15.00,
                                                    fontWeight:
                                                        FontWeight.bold))
                                          ],
                                        )
                                      ],
                                    )
                                  ]),
                            ),
                          ),
                        );
                        /* ListTile(
                            leading: ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: Image(
                                fit: BoxFit.cover,
                                image: NetworkImage(oListaProducto[index]
                                    .images![0]
                                    .toString()),
                              ),
                            ),
                            title: Text(
                                oListaProducto[index].title.toString().trim()),
                            subtitle: Text(oListaProducto[index]
                                .description
                                .toString()
                                .trim()),
                            contentPadding:
                                EdgeInsets.symmetric(vertical: 50.0));*/
                        /* Column(children: [
                          Row(children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Stack(children: [
                                Container(
                                    width: 170,
                                    height: 200,
                                    //color: Colors.amber,
                                    decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius:
                                          BorderRadius.circular(15.00),
                                    ),
                                    child:
                                        ((oListaProducto[index].images!.isEmpty
                                            ? Container()
                                            : ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(25),
                                                child: Image(
                                                  fit: BoxFit.cover,
                                                  image: NetworkImage(
                                                      oListaProducto[index]
                                                          .images![0]
                                                          .toString()),
                                                ),
                                              )))),
                                Positioned(
                                  top: 10,
                                  left: 100,
                                  child: Container(
                                    width: 50,
                                    height: 50,
                                    //color: Colors.amber,
                                    decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      shape: BoxShape.circle,
                                    ),
                                    child: const Icon(Icons.favorite,
                                        color: Colors.black, size: 30),
                                  ),
                                ),
                              ]),

                              /*  radius: 40,
                            backgroundImage: NetworkImage(
                                oListaCategoria[index].image.toString()),*/
                            ),
                          ]),
                          const SizedBox(
                            height: 10,
                          ),
                          Column(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    oListaProducto[index]
                                        .title
                                        .toString()
                                        .trim()
                                        .substring(
                                            0,
                                            (oListaProducto[index]
                                                        .title
                                                        .toString()
                                                        .length >
                                                    10
                                                ? 10
                                                : oListaProducto[index]
                                                    .title
                                                    .toString()
                                                    .length)),
                                    // textAlign: TextAlign.left,
                                    style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 17.00,
                                        fontWeight: FontWeight.bold)),
                                Text('S/ ${oListaProducto[index].price}',
                                    // textAlign: TextAlign.left,
                                    style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 17.00,
                                        fontWeight: FontWeight.bold)),
                              ]),
                        ]);*/
                      })),
                ))
              : LinearProgressIndicator()),
        ),
        Total(
          nSize: nSize,
          nTotal: lnTotal,
        )
        /* Busqueda(nSize: nSize),
        Promocion(nSize: nSize),
        CategoriaInicio(nSize: nSize),*/
      ],
    );
  }
}
