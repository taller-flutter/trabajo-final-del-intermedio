import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../themes/colors_app.theme.dart';

class NovedadesPage extends StatelessWidget {
  const NovedadesPage({super.key});

  @override
  Widget build(BuildContext context) {
    final nSize = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          //  padding: EdgeInsets.symmetric(horizontal: 100.00),
          width: double.infinity,
          height: nSize.height * 0.5,
          decoration: const BoxDecoration(
              gradient: ColorsApp.oColorAlternoGradiente,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30.00),
                bottomRight: Radius.circular(30.00),
              ),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, blurRadius: 15, offset: Offset(0, 5))
              ]),
        ),
        Padding(
          padding:
              EdgeInsets.only(top: nSize.height * 0.10, left: 20, right: 20),
          child: const Flexible(
            child: Text('Todas las Novedades del Mundo de la Moda',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.00,
                    fontWeight: FontWeight.bold)),
          ),
        ),
        SizedBox(
          // alignment: Alignment.topLeft,
          width: double.infinity,
          height: nSize.height * 0.5,
          //color: Colors.red,
          child: const Image(
            alignment: Alignment.bottomCenter,
            image: AssetImage('assets/novedades.png'),
            //fit: BoxFit.cover,
            //  height: 100,
            //  width: 100,
          ),
        ),
      ],
    );
  }
}
