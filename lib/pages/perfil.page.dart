import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../themes/colors_app.theme.dart';

class PerfilPage extends StatelessWidget {
  const PerfilPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 100, left: 20, right: 20),
      child: Align(
        child: Column(
          children: [
            const CircleAvatar(
              radius: 100,
              backgroundImage: NetworkImage(
                  'https://thumbs.dreamstime.com/b/artista-de-sexo-femenino-hermoso-en-su-estudio-59656912.jpg'),
            ),
            const SizedBox(
              height: 20.0,
            ),
            const Text(
              'LUCERO LUANA',
              style: TextStyle(
                color: ColorsApp.oColorAlterno,
                fontSize: 18.00,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  decoration: BoxDecoration(
                    // color: Colors.white,
                    borderRadius: BorderRadius.circular(30.00),
                  ),
                  child: IconButton(
                    color: ColorsApp.oColorAlterno,
                    iconSize: 30.00,
                    onPressed: () {},
                    icon: const Icon(Icons.home_work),
                  )),
              const Text(
                'PERU',
                style: TextStyle(
                  color: ColorsApp.oColorAlterno,
                  fontSize: 18.00,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ]),
            const SizedBox(
              height: 20.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  decoration: BoxDecoration(
                    // color: Colors.white,
                    borderRadius: BorderRadius.circular(30.00),
                  ),
                  child: IconButton(
                    color: ColorsApp.oColorAlterno,
                    iconSize: 30.00,
                    onPressed: () {},
                    icon: const Icon(Icons.home),
                  )),
              const Text(
                'TINGO MARIA',
                style: TextStyle(
                  color: ColorsApp.oColorAlterno,
                  fontSize: 18.00,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}
