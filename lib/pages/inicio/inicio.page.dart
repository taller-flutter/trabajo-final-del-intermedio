import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:practicafinal/pages/inicio/widgets/inicio.widget.dart';

import '../../models/models.dart';
import '../../themes/colors_app.theme.dart';

class InicioPage extends StatefulWidget {
  const InicioPage({super.key});

  @override
  State<InicioPage> createState() => _InicioPageState();
}

class _InicioPageState extends State<InicioPage> {
  @override
  Widget build(BuildContext context) {
    final nSize = MediaQuery.of(context).size;

    return Stack(
      children: [
        Container(
          color: ColorsApp.oColorFondo,
        ),
        Cabecera(nSize: nSize),
        Busqueda(nSize: nSize),
        Promocion(nSize: nSize),
        CategoriaInicio(nSize: nSize),
      ],
    );
  }
}
