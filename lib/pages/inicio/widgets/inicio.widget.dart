export 'package:practicafinal/pages/inicio/widgets/producto_inicio.widget.dart';

export 'package:practicafinal/pages/inicio/widgets/categoria_inicio.widget.dart';

export 'package:practicafinal/pages/inicio/widgets/promocion.widget.dart';

export 'package:practicafinal/pages/inicio/widgets/busqueda.widget.dart';

export 'package:practicafinal/widgets/cabecera.widget.dart';
