import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../../models/models.dart';
import '../../../themes/colors_app.theme.dart';
import 'inicio.widget.dart';

class CategoriaInicio extends StatefulWidget {
  CategoriaInicio({super.key, required this.nSize});

  final Size nSize;

//  final VoidCallback oAccion;

  @override
  State<CategoriaInicio> createState() => _CategoriaInicioState();
}

class _CategoriaInicioState extends State<CategoriaInicio> {
  @override
  void initState() {
    obtenerCategoriaInicio();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<Categoria> oListaCategoria = [];
  List<Producto> oListaProducto = [];
  int nId = 0;
  int nIdCategoria = 0;

  void obtenerCategoriaInicio() async {
    var dio = Dio();
    final response =
        await dio.get('https://api.escuelajs.co/api/v1/categories');

    oListaCategoria =
        List<Categoria>.from(response.data.map((x) => Categoria.fromJson(x)));

    nIdCategoria = oListaCategoria[0].id!;
    oListaCategoria[0].lIdSelecionado = true;

    if (this.mounted) {
      obtenerProducto(nIdCategoria.toString());
      setState(() {});
    }
    //setState(() {});
  }

  void obtenerProducto(String nCategoria) async {
    var dio = Dio();
    final response = await dio
        .get('https://api.escuelajs.co/api/v1/categories/$nCategoria/products');

    // oListaEmpresa:List<Empresa>.from(oNuevaListaEmpresa.map((x) => Empresa.fromJson(x))),

    oListaProducto =
        List<Producto>.from(response.data.map((x) => Producto.fromJson(x)));

    //  print('obtenerProducto');
    //  print(oListaProducto);

    if (this.mounted) {
      print('*************mounted*****************');
      print(nCategoria);
      print('******************************');
      // check whether the state object is in tree
      setState(() {
        // make changes here
      });
    }
    //setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        // color: Colors.red,
        /*child: Padding(
            padding: EdgeInsets.only(top: widget.nSize.height * 0.1),
            child: Container(),
          )*/
        child: Padding(
          padding: EdgeInsets.only(
              top: widget.nSize.height * 0.35, left: 20, right: 20),
          child: (oListaCategoria.isNotEmpty
              ? SingleChildScrollView(
                  child: SizedBox(
                  height: 50.0,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: oListaCategoria.length,
                      itemBuilder: ((context, index) {
                        return Row(children: [
                          InkWell(
                            onTap: () {
                              List<Producto> oListaProducto = [];
                              oListaCategoria.forEach((i) {
                                i.lIdSelecionado = false;
                              });

                              oListaCategoria[index].lIdSelecionado = true;

                              ///nIdCategoria = oListaCategoria[index].id;
                              setState(() {
                                nIdCategoria = oListaCategoria[index].id!;

                                obtenerProducto(
                                    oListaCategoria[index].id.toString());
                              });
                            },
                            child: Container(
                              width: 80,
                              decoration: BoxDecoration(
                                  color: (oListaCategoria[index].lIdSelecionado!
                                      ? ColorsApp.oColorAlterno
                                      : null),
                                  borderRadius: BorderRadius.circular(8.00)),
                              child: Text(
                                  (oListaCategoria[index]
                                              .name
                                              .toString()
                                              .length >
                                          8
                                      ? oListaCategoria[index]
                                          .name
                                          .toString()
                                          .substring(0, 8)
                                      : oListaCategoria[index].name.toString()),
                                  style: const TextStyle(
                                      // color: Colors.blueAccent,
                                      fontSize: 18.00,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                        ]);
                      })),
                ))
              : Padding(
                  padding: EdgeInsets.only(top: widget.nSize.height * 0.05),
                  child: const LinearProgressIndicator())),
        ),
      ),
      ProductoInicio(nSize: widget.nSize, oListaProducto: oListaProducto)
    ]);
  }
}
