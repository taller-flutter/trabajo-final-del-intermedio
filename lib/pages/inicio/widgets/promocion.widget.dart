import 'package:flutter/material.dart';

import '../../../themes/colors_app.theme.dart';

class Busqueda extends StatelessWidget {
  const Busqueda({
    Key? key,
    required this.nSize,
  }) : super(key: key);

  final Size nSize;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(top: nSize.height * 0.18, left: 20.00, right: 20.00),
      child: Stack(children: [
        Padding(
          padding: EdgeInsets.only(top: nSize.height * 0.02),
          child: Container(
            //  padding: EdgeInsets.symmetric(horizontal: 100.00),
            width: double.infinity,
            height: nSize.height * 0.15,
            decoration: BoxDecoration(
                gradient: ColorsApp.oColorAlternoGradiente,
                borderRadius: BorderRadius.circular(30.00)),
          ),
        ),
        Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const SizedBox(
            width: 10.0,
          ),
          Expanded(
            child: Container(
              alignment: Alignment.topLeft,
              width: double.infinity,
              height: nSize.height * 0.17,
              //color: Colors.red,
              child: const Image(
                image: AssetImage('assets/oferta4.png'),
                //fit: BoxFit.cover,
                //  height: 100,
                //  width: 100,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: nSize.height * 0.04, left: 0.00, right: 20.00),
            child: Column(
              children: const [
                Text('GRAN OFERTA',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    )),
                SizedBox(
                  width: 120,
                  height: 100,
                  child: Text(
                      'Precios de Locura, con un descuento del 50 % en marcas exclusivas.',
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.white,
                      )),
                )
              ],
            ),
          )
        ])
      ]),
    );
  }
}
