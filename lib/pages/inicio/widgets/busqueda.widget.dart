import 'package:flutter/material.dart';

class Promocion extends StatelessWidget {
  const Promocion({
    Key? key,
    required this.nSize,
  }) : super(key: key);

  final Size nSize;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(top: nSize.height * 0.1, left: 20.00, right: 20.00),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            height: 45,
            width: nSize.width * 0.10,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18),
              border: Border.all(
                width: 1.0,
                color: Colors.grey,
                //color: Colors.white,
              ),
            ),
            child: IconButton(
              // style:ButtonStyle(backgroundColor:),
              iconSize: 35.00,
              color: const Color.fromRGBO(178, 181, 192, 1.0),
              onPressed: () {},
              icon: const Icon(Icons.search),
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Container(
          decoration: const BoxDecoration(
              color: Color.fromRGBO(253, 176, 188, 1.0),
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12, blurRadius: 15, offset: Offset(0, 5))
              ]),
          child: IconButton(
            iconSize: 25.00,
            color: Colors.white,
            onPressed: () {},
            icon: const Icon(Icons.tune_outlined),
          ),
        ),
      ]),
    );
  }
}
