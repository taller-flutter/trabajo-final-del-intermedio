import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../../models/models.dart';

class ProductoInicio extends StatefulWidget {
  const ProductoInicio(
      {super.key, required this.nSize, required this.oListaProducto});
  //final List<Producto> widget.oListaProducto;
  final Size nSize;
  final List<Producto> oListaProducto;

  @override
  State<ProductoInicio> createState() => _ProductoInicioState();
}

class _ProductoInicioState extends State<ProductoInicio> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 20.00, right: 20.00),
      child: (widget.oListaProducto.isNotEmpty
          ? SingleChildScrollView(
              child: SizedBox(
              height: widget.nSize.height * 0.45,
              child: GridView.builder(
                  padding: const EdgeInsets.only(top: 10.0),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    mainAxisExtent: 350,
                  ),
                  itemCount: widget.oListaProducto.length,
                  itemBuilder: ((context, index) {
                    return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ((widget.oListaProducto[index].images!.isEmpty
                              ? Container()
                              : Flexible(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: Image(
                                      // width: 100,
                                      height: 300,
                                      fit: BoxFit.cover,
                                      image: NetworkImage(widget
                                          .oListaProducto[index].images![0]
                                          .toString()),
                                    ),
                                  ),
                                ))),
                          const SizedBox(
                            height: 10,
                          ),
                          Column(
                              //crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    widget.oListaProducto[index].title
                                        .toString()
                                        .trim()
                                        .substring(
                                            0,
                                            (widget.oListaProducto[index].title
                                                        .toString()
                                                        .length >
                                                    10
                                                ? 10
                                                : widget
                                                    .oListaProducto[index].title
                                                    .toString()
                                                    .length)),
                                    // textAlign: TextAlign.left,
                                    style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15.00,
                                        fontWeight: FontWeight.bold)),
                                Text('S/ ${widget.oListaProducto[index].price}',
                                    // textAlign: TextAlign.left,
                                    style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 17.00,
                                        fontWeight: FontWeight.bold)),
                              ]),
                        ]);
                  })),
            ))
          : const LinearProgressIndicator()),
    );
  }
}
