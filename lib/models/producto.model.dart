import 'dart:convert';

import 'categoria.model.dart';

class Producto {
  Producto({
    this.id,
    this.title,
    this.price,
    this.description,
    this.category,
    this.images,
  });

  int? id;
  String? title;
  int? price;
  String? description;
  Categoria? category;
  List<String>? images;

  factory Producto.fromRawJson(String str) =>
      Producto.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Producto.fromJson(Map<String, dynamic> json) => Producto(
        id: json["id"],
        title: json["title"],
        price: json["price"],
        description: json["description"],
        category: Categoria.fromJson(json["category"]),
        images: List<String>.from(json["images"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "price": price,
        "description": description,
        "category": category!.toJson(),
        "images": List<dynamic>.from(images!.map((x) => x)),
      };
}
