import 'dart:convert';

class Categoria {
  Categoria({
    this.id,
    this.name,
    this.image,
  });

  int? id;
  String? name;
  String? image;
  bool? lIdSelecionado = false;

  factory Categoria.fromRawJson(String str) =>
      Categoria.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Categoria.fromJson(Map<String, dynamic> json) => Categoria(
        id: json["id"],
        name: json["name"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
      };
}
